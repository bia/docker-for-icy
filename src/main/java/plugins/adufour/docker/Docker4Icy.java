package plugins.adufour.docker;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import icy.file.FileUtil;
import icy.plugin.PluginLoader;
import icy.plugin.abstract_.Plugin;
import icy.plugin.classloader.JarClassLoader;
import icy.plugin.interface_.PluginDaemon;
import icy.util.JarUtil;

/**
 * Icy interface to <a href="https://www.docker.com/what-docker">Docker</a>. This daemon loads up
 * necessary 3rd-party libraries into the class path at startup (and every time the plug-in list is
 * reloaded). To interact with Docker, see the {@link DockerUtil} class.
 * 
 * @author Alexandre Dufour
 */
public class Docker4Icy extends Plugin implements PluginDaemon
{
    /** The temporary folder where the 3rd party libraries will be extracted */
    private static final String libFolder = FileUtil.getTempDirectory() + "/Docker4Icy/";
    
    @Override
    public void init()
    {
        try
        {
            // 1) Unpack and load 3rd-party JAR libraries via a temporary folder
            URL url = DockerUtil.class.getResource("lib");
            if (url != null)
            {
                String[] jarFiles = new File(url.getFile()).list();
                for (String jarFile : jarFiles)
                {
                    String fileName = FileUtil.getFileName(jarFile);
                    extractResource(libFolder + fileName, DockerUtil.class.getResource("lib" + File.separator + fileName));
                    ((JarClassLoader) PluginLoader.getLoader()).add(new URL("file://" + libFolder + fileName));
                }
            }
            else
            {
                String jarPath = FileUtil.getApplicationDirectory() + "/" + getDescriptor().getJarFilename();
                for (String jarFile : JarUtil.getAllFiles(jarPath, false, false))
                    if (jarFile.endsWith(".jar"))
                    {
                        String fileName = FileUtil.getFileName(jarFile);
                        extractResource(libFolder + fileName, DockerUtil.class.getResource("lib/" + fileName));
                        ((JarClassLoader) PluginLoader.getLoader()).add(libFolder + fileName);
                    }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    public void run()
    {
        // Don't run. I'm too fast for you anyway.
    }
    
    @Override
    public void stop()
    {
        // Delete the temporary folder:
        // 1) for the beauty of saving space until the next run...
        // 2) To prevent potential conflicts after upgrades
        FileUtil.delete(new File(libFolder), true);
    }
    
}
