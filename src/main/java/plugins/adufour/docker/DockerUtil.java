package plugins.adufour.docker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerCmd;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.AccessMode;
import com.github.dockerjava.api.model.Bind;
import com.github.dockerjava.api.model.Volume;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.command.ExecStartResultCallback;
import com.github.dockerjava.core.command.PullImageResultCallback;

/**
 * Utility class to easily interface with (and run)
 * <a href="https://www.docker.com/what-docker">Docker</a> within Icy, using the official
 * <a href="https://github.com/docker-java/docker-java">docker-java API</a>.
 * 
 * @author Alexandre Dufour
 */
public class DockerUtil
{
    private static DockerClient docker = DockerClientBuilder.getInstance(DefaultDockerClientConfig.createDefaultConfigBuilder().build()).build();
    
    /**
     * Starts a container with the specified image
     * 
     * @param image
     *            the image to run (and download if necessary)
     * @return the container ID
     * @throws InterruptedException
     */
    public static String startContainer(String image) throws InterruptedException
    {
        return startContainer(image, (List<Bind>) null, null);
    }
    
    /**
     * Starts a container with the specified image and virtual folder binding(s)
     * 
     * @param image
     *            the image to run (and download if necessary)
     * @param bindings
     *            Virtual bindings between host and container folders, e.g.
     *            <code>{"/host/a", "/docker/b"}</code>. NB: by default, these mappings are set to
     *            "read-write" mode. For further control, use
     *            {@link #startContainer(String, List, String)}
     * @return the container ID
     * @throws InterruptedException
     */
    public static String startContainer(String image, Map<String, String> bindings) throws InterruptedException
    {
        return startContainer(image, bindings, null);
    }
    
    /**
     * Starts a container with the specified image and virtual folder binding(s)
     * 
     * @param image
     *            the image to run (and download if necessary)
     * @param bindings
     *            Virtual bindings between host and container folders, e.g.
     *            <code>new Bind("/host/a", "/docker/b", AccessMode.rw)</code>
     * @return the container ID
     * @throws InterruptedException
     */
    public static String startContainer(String image, List<Bind> bindings) throws InterruptedException
    {
        return startContainer(image, bindings, null);
    }
    
    /**
     * Starts a container with the specified image and virtual folder binding(s)
     * 
     * @param image
     *            the image to run (and download if necessary)
     * @param bindings
     *            Virtual bindings between host and container folders, e.g.
     *            <code>{"/host/a", "/docker/b"}</code>. NB: by default, these mappings are set to
     *            "read-write" mode. For further control, use
     *            {@link #startContainer(String, List, String)}
     * @param workingDirectory
     *            the directory where the container should start from (e.g.
     *            <code>"/docker/b/subdir"</code>). This is equivalent to (but faster than) starting
     *            the container and then calling <code>"cd /docker/b/subdir"</code>
     * @return the container ID
     * @throws InterruptedException
     */
    public static String startContainer(String image, Map<String, String> bindings, String workingDirectory) throws InterruptedException
    {
        List<Bind> bindingList = null;
        
        if (bindings != null)
        {
            // Convert user mappings to Docker bindings
            bindingList = new ArrayList<Bind>(bindings.size());
            for (Entry<String, String> binding : bindings.entrySet())
                bindingList.add(new Bind(binding.getKey(), new Volume(binding.getValue()), AccessMode.rw));
        }
        
        return startContainer(image, bindingList, workingDirectory);
    }
    
    /**
     * Starts a container with the specified image and virtual folder binding(s)
     * 
     * @param image
     *            the image to run (and download if necessary)
     * @param bindings
     *            Virtual bindings between host and container folders, e.g.
     *            <code>new Bind("/host/a", "/docker/b", AccessMode.rw)</code>
     * @param workingDirectory
     *            the directory where the container should start from (e.g.
     *            <code>"/docker/b/subdir"</code>). This is equivalent to (but faster than) starting
     *            the container and then calling <code>"cd /docker/b/subdir"</code>
     * @return the container ID
     * @throws InterruptedException
     */
    public static String startContainer(String image, List<Bind> bindings, String workingDirectory) throws InterruptedException
    {
        System.out.println("Fetching image " + image);
        docker.pullImageCmd(image).exec(new PullImageResultCallback()).awaitCompletion();
        
        System.out.println("Starting container using " + image);
        
        CreateContainerCmd createCommand = docker.createContainerCmd(image);
        
        if (bindings != null) createCommand = createCommand.withBinds(bindings);
        
        if (workingDirectory != null) createCommand = createCommand.withWorkingDir(workingDirectory);
        
        // Emulate a TTY and attach output and error streams
        createCommand = createCommand.withTty(true).withAttachStdout(true).withAttachStderr(true);
        
        // Finally, start the container
        CreateContainerResponse container = createCommand.exec();
        String containerID = container.getId();
        docker.startContainerCmd(containerID).exec();
        
        return containerID;
    }
    
    public static void stopContainer(String containerID)
    {
        docker.stopContainerCmd(containerID).exec();
    }
    
    /**
     * Runs the specified command in the given container and awaits completion
     * 
     * @param containerID
     *            the ID of the container where the command should run
     * @param command
     *            the command to run (e.g. <code>"echo hello Icy world!"</code>
     * @throws InterruptedException
     *             if the calling thread was interrupted before completion of the command
     */
    public static void runCommand(String containerID, String command) throws InterruptedException
    {
        String cmd = docker.execCreateCmd(containerID).withAttachStdout(true).withAttachStderr(true).withTty(true).withCmd(command.split(" ")).exec().getId();
        docker.execStartCmd(cmd).exec(new ExecStartResultCallback(System.out, System.err)).awaitCompletion();
    }
}
